<?php
/**
 * @author Mailjet SAS
 *
 * @copyright  Copyright (C) 2014 Mailjet SAS.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */
defined('_JEXEC') or die('Restricted access');

$lib_dir = __DIR__.'/../../../lib/';

require_once ($lib_dir.'config.php');
require_once ($lib_dir.'lib/Auth.php');
require_once ($lib_dir.'lib/mailjet-api-strategy.php');

$auth = new Auth();

$nextStepUrl = $auth->haveToken() ? 'campaigns' : 'reseller/signup';

//$address = "{$mailjetUrl}/{$nextStepUrl}?r={$resellerName}&show_menu=none&u=Joomla-3.0&f=amc";
$address = "https://app.mailjet.com/{$nextStepUrl}?r={$resellerName}&show_menu=none&u=Joomla-3.0&f=amc";

if ($auth->haveToken()) {
    $address .= "&t=" . $auth->getToken();
}
?>

<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
    <iframe width="1000" height="1300" src="<?php echo $address; ?>">
</div>